#pragma once

#include <SFML/Graphics/Texture.hpp>
#include <unordered_map>
#include <memory>

class TextureManager
{
public:
	
	inline static TextureManager& Instance()
	{
		static TextureManager ins;
		return ins;
	}

	static bool Load( const char* name, const char* path )
	{
		auto& man = TextureManager::Instance();
		auto& result = man.Get( name );
		if ( !result ) {
			result = std::make_unique<sf::Texture>();
		}

		if ( result->loadFromFile( path ) ) {

			man.data[name] = std::move(result);

			return true;
		}

		return false;
	}

	static std::unique_ptr<sf::Texture>& Get( const char* name )
	{
		static std::unique_ptr<sf::Texture> not_found = std::unique_ptr<sf::Texture>(nullptr);

		auto& man = TextureManager::Instance();
		auto result = man.data.find( name );
		if ( result == man.data.end() ) return not_found;

		return result->second;
	}

private:
	TextureManager() = default;

	std::unordered_map<const char*, std::unique_ptr<sf::Texture>> data;

};