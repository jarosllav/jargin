#pragma once

#include "Entity.hpp"
#include <vector>
#include <memory>

class EntityManager
{
public:
	void Update()
	{
		for ( auto& e : entities )
			e->Update();
	}
	
	void Draw(sf::RenderWindow* win)
	{
		for ( auto& e : entities )
			e->Draw(win);
	}

	void Refresh()
	{
		entities.erase( std::remove_if( std::begin( entities ), std::end( entities ), []( const std::unique_ptr<Entity> &entity ) {
			return !entity->IsActive();
		} ), std::end( entities ) );
	}

	Entity& AddEntity()
	{
		Entity* e = new Entity();
		std::unique_ptr<Entity> entity{ e };
		entities.emplace_back( std::move( entity ) );
		return *e;
	}

private:
	std::vector<std::unique_ptr<Entity>> entities;

};