#pragma once

#include <vector>
#include <memory>
#include <algorithm>
#include <SFML/System/Vector2.hpp>

#include "Defines.hpp"
#include "Component.hpp"

class Entity
{
public:
	
	void Update()
	{
		for ( auto& c : components ) 
			c->Update();
	}

	void Draw(sf::RenderWindow* win)
	{
		for ( auto& c : components ) 
			c->Draw(win);
	}

	inline bool IsActive() const { return active; }
	inline void Deactive() { active = false; }

	template<class T> bool HasComponent() const
	{
		return componentBitset[getComponentID<T>()];
	}

	template<class T, typename... TArgs>
	T& AddComponent( TArgs&&... args )
	{
		T* c( new T( std::forward<TArgs>( args )... ));
		c->entity = this;
		std::unique_ptr<Component> ptr{ c };
		components.emplace_back( std::move( ptr ) );

		componentArray[getComponentID<T>()] = c;
		componentBitset[getComponentID<T>()] = true;

		c->Init();
		return *c;
	}

	template<class T>
	T& GetComponent() const
	{
		auto c( componentArray[getComponentID<T>()] );
		return *static_cast<T*>( c );
	}

	//Position
	void SetPosition( float x, float y )
	{
		this->position.x = x;
		this->position.y = y;
	}
	void SetPosition( const sf::Vector2f& pos )
	{
		SetPosition( pos.x, pos.y );
	}
	void Move(float x, float y)
	{
		this->position.x += x;
		this->position.y += y;
	}
	void Move( const sf::Vector2f& move )
	{
		Move( move.x, move.y );
	}
	sf::Vector2f GetPosition()
	{
		return this->position;
	}
	//Size
	void SetSize( float x, float y )
	{
		this->size.x = x;
		this->size.y = y;
	}
	void SetSize( const sf::Vector2u& size )
	{
		SetSize( size.x, size.y );
	}
	sf::Vector2u GetSize()
	{
		return this->size;
	}
private:

	bool active = true;
	std::vector<std::unique_ptr<Component>> components;

	ComponentBitSet componentBitset;
	ComponentArray componentArray;

	sf::Vector2f position;
	sf::Vector2u size;

};