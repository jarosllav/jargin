#pragma once

#include <SFML/Graphics/Rect.hpp>

namespace Collision
{

	static bool Rect( const sf::IntRect& A, const sf::IntRect& B )	
	{
		if ( A.left + A.width >= B.left && B.left + B.width >= A.left
			 && A.top + A.height >= B.top && B.top + B.height >= A.top )
			return true;
		return false;
	}

	static bool Circle()
	{

	}


};

